import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import {store} from "./app/store";
import Home from './features/Home';
function App() {
  return (
      <BrowserRouter>
        <Switch>
            <Route store={store} component={Home} />
        </Switch>
      </BrowserRouter>
  );
}

export default App;
