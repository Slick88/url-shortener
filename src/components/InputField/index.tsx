import React, { Component } from 'react';
import { Dropdown, Form } from 'react-bootstrap';
import { isUndefined } from 'lodash';
import './InputField.css';
export interface Props {
    label?: string;
    name?: string;
    placeholder?: string;
    onChange(arg0: any): void;
    value: string | undefined;
    instruction?: string;
    fieldType?: 'input' | 'dropdown';
    options?: { key: string; value: string }[];
    isValid?: boolean;
    errorMessage?: string;
    disabled?: boolean;
}

class InputField extends Component<Props> {
    renderDropdown = () => {
        const { onChange, value, options, isValid } = this.props;
        const getValue = () => {
            // @ts-ignore
            const selected = options.find((option) => option.key === value);
            // @ts-ignore
            return selected ? selected.value : 'Please select an option';
        };

        return (
            <Dropdown onSelect={onChange}>
                <Dropdown.Toggle bsPrefix={`${isValid === true && 'valid'}`} id="dropdown-basic">
                    {getValue()}
                </Dropdown.Toggle>

                <Dropdown.Menu style={{ width: '100%' }}>
                    {!isUndefined(options) &&
                    options.map((option) => (
                        <Dropdown.Item key={option.key} active={value === option.value} eventKey={option.key}>
                            {option.value}
                        </Dropdown.Item>
                    ))}
                </Dropdown.Menu>
            </Dropdown>
        );
    };

    renderInputField = () => {
        const { onChange, value, isValid, placeholder,disabled } = this.props;
        return (
            <Form.Control
                width={50}
                type="text"
                onChange={onChange}
                value={value}
                isValid={isValid === true}
                isInvalid={isValid === false}
                placeholder={placeholder}
                disabled={disabled}
            />
        );
    };

    renderLabel = () => {
        const { label } = this.props;

        return (
            <Form.Label className="input-field-label" column={true}>
                {label}
            </Form.Label>
        )
    }
    render() {
        const { label, name, instruction } = this.props;
        return (
            <div>
                <Form.Group controlId={name}>
                    {label && this.renderLabel()}
                    {this.props.fieldType === 'dropdown' && this.renderDropdown()}
                    {this.props.fieldType !== 'dropdown' && this.renderInputField()}
                    {instruction && <Form.Text>{instruction}</Form.Text>}
                    <Form.Control.Feedback type="invalid">{this.props.errorMessage}</Form.Control.Feedback>
                </Form.Group>
            </div>
        );
    }
}

export default InputField;
