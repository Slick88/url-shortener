import React, {Component} from 'react';
import { Navbar } from "react-bootstrap";

class NavBar extends Component {
    render() {
        return (
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="#home">Welcome to Satish Url Shortener</Navbar.Brand>
            </Navbar>
        );
    }
}

export default NavBar;