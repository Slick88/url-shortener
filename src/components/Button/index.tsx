import React, { Component } from 'react';
import { Button as RBButton, ButtonProps } from 'react-bootstrap';

export interface Props {
    onClick?(arg0: any): void;
    variant?: any;
    children: string;
    type?: 'button' | 'reset' | 'submit';
    block?: boolean;
    disabled?: ButtonProps['disabled'];
}

class Button extends Component<Props> {
    render() {
        const { children } = this.props;
        return (
            <>
                <RBButton {...this.props}>{children}</RBButton>
            </>
        );
    }
}

export default Button;
