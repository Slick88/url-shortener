import React  from 'react';
import { Container } from 'react-bootstrap'
import Navbar from '../components/NavBar';

export interface Props {
    location: {
        pathname: string;
    };
}

const PageWrapper = (WrapperComponents: any) => {
    return (props: Props) => {
        return (
            <>
                <Navbar/>
                <Container fluid>
                    <WrapperComponents {...props} />
                </Container>
             </>
        );
    };
};

export default PageWrapper;
