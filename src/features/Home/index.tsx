import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import copy from 'copy-to-clipboard';
import PageWrapper from "../../containers/PageWrapper";
import InputField from "../../components/InputField";
import { updateLongUrl, shortenUrl, getShortUrl, getLongUrl } from './homeSlice';
import Button from "../../components/Button";
import { Form, Col, InputGroup } from 'react-bootstrap';

export const Home = () => {
    const dispatch = useDispatch();
    const longUrl = useSelector(getLongUrl)
    const shortUrl = useSelector(getShortUrl)

    const renderShortUrl = () => (
        <InputGroup className="short-url-group">
            <Form.Control
                value={shortUrl}
                readOnly
            />
            <InputGroup.Append>
                <Button variant="outline-secondary" onClick={() => {copy(shortUrl);}}>Click to Copy</Button>
            </InputGroup.Append>
        </InputGroup>
    )
    return (
        <Form>
            <Form.Row>
                <Col lg={10}>
                    <InputField placeholder={'Enter Your link to shorten'} name={'longUrl'} onChange={(e) => dispatch(updateLongUrl(e.target.value))} value={longUrl}/>
                </Col>
                <Col>
                    <Button onClick={() => dispatch(shortenUrl(longUrl))}>Shorten URL</Button>
                </Col>
            </Form.Row>
            {shortUrl && renderShortUrl()}
        </Form>
    );
}

export default PageWrapper(Home);