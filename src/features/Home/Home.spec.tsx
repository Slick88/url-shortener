import React from "react";
import {mount} from 'enzyme';
import {Provider} from "react-redux";
import {Home} from "./index";
import {store} from "../../app/store";
import InputField from "../../components/InputField";
import Button from "../../components/Button";
import {InputGroup} from "react-bootstrap";
let HomeWrapper: any

const render = () => mount(
    <Provider store={store}>
        <Home/>
    </Provider>
);

describe('Init Home page', () => {
    beforeEach(() => {
        HomeWrapper = render();
    })
    it('should render page',  () => {
        expect(HomeWrapper).toMatchSnapshot()
        // console.log(HomeWrapper.debug())
    });
    it('should have a single correct InputField', function () {
        expect(HomeWrapper.find(InputField)).toHaveLength(1)
        expect(HomeWrapper.find(InputField).prop('placeholder')).toEqual('Enter Your link to shorten');
    });
    it('should have a Button', function () {
        expect(HomeWrapper.find(Button)).toHaveLength(1)
    });

    it('should not render short name fields', function () {
        expect(HomeWrapper.find(InputGroup).exists()).toEqual(false);
    });
})
