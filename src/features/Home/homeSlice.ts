import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {AppThunk, RootState} from '../../app/store';
import axios from "axios";

interface HomeState {
  value: number;
}

const initialState = {
  longUrl: "",
  shortUrl: ""
};

export const homeSlice = createSlice({
  name: 'home',
  initialState,
  reducers: {
    updateShortUrl: (state, action: PayloadAction<string>) => {
      state.shortUrl = action.payload;
    },
    updateLongUrl: (state,action: PayloadAction<string>) => {
      state.longUrl = action.payload;
    },

  },
});

export const getShortUrl= (state: RootState) => state.home.shortUrl;
export const getLongUrl= (state: RootState) => state.home.longUrl;

export const { updateShortUrl, updateLongUrl } = homeSlice.actions;

export const shortenUrl = (longUrl: string):AppThunk => (dispatch) => {
  const url = process.env.REACT_APP_API_ENDPOINT
  return axios.post(`${url}/shortlink`, {longUrl})
      .then((response) => {
        console.log(response);
        dispatch(updateShortUrl(response.data.shortUrl));
      })
      .catch((e) => {
        console.log(e);
      });
};

export default homeSlice.reducer;
